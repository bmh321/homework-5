# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp

	(λp.(pz))(λq.w(λw.(wqzp)))

2. λp.pq λp.qp

	λp.((pq)(λp.(qp))

## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q
	-First Lambda: The S's are bound to each other, and the Z is free
	-Second Lambda: The Q's are bound to each other, and the S is free completely (different from the First Lambda's S's)

2. (λs. s z) λq. w λw. w q z s
	-First Lambda: The S's are bound and the Z is free
	-Second Lambda: The Q's are bound to each other, the W is free
	-Third Lambda: The W's are bound to each other, but not to the one left of the Lambda, the Z and S are free, the S is free of the First Lambda as well

3. (λs.s) (λq.qs)
	-First Lambda: The S's are bound
	-Second Lambda: The Q's are bound, the S is free of both Lambda's

4. λz. (((λs.sq) (λq.qz)) λz. (z z))
	-First Lambda: The S's are bound
	-Second Lambda: The Q's are bound, the Z is free
	-Third Lambda: The Z's are bound to each other; but the Z in the Second Lambda is free from them

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)
	[z -> λq.q q]
	(λq.q q)(λs.s a)
	[q -> λs.s a]
	(λs.s a)(λs.s a)

2. (λz.z) (λz.z z) (λz.z q)
	[z -> λz.z z] //Not the same Z's
	(λz.z z)(λz.z q)
	[z -> λz.z q] //Not the same Z's
	(λz.z q)(λz.z q)

3. (λs.λq.s q q) (λa.a) b
	[s -> λa.a]
	(λq.λa.a q q) b
	[q -> b]
	λa.a b b

4. (λs.λq.s q q) (λq.q) q
	(λq.q) -> (λa.a) //alpha conversion
	[s -> λa.a]
	(λq.λa.a q q) q
	q -> b		 //alpha conversion
	[q -> b]
	λa.a b b
	
5. ((λs.s s) (λq.q)) (λq.q)
	[s -> λq.q]
	(λq.q λq.q)(λq.q)
	q -> a 		//alpha conversion
	[q -> λa.a]
	λa.a λa.a  	//Since they're bound Q's im assuming that both λq's fall out and both q's are Beta reduced

## Question 4

1. Write the truth table for the or operator below.
	a	b 	(a V b)
    ------------------------------
	T	T	   T
	T	F	   T
	F	T	   T
	F	F	   F

2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 

a) a = T; b = T

T = λa.λb.a
F = λa.λb.b
OR = λp.λq.p p q

(λp.λq.p p q)(λa.λb.a)(λa.λb.a)
[p-> λa.λb.a]
λq.λa.λb.a λa.λb.a q (λa.λb.a)
[q-> λa.λb.a]
(λa.λb.a λa.λb.a) λa.λb.a //The first two are bound a's and b's but the last is not the same a and b
[a -> λa'.λb'.a']
λb.λa'.λb'.a' λb.λa'.λb'.a'
[b-> λb'.λa''.λb''.a'']
λa.λb.a == T

b) a = T; b = F

(λp.λq.p p q) T F
[p -> T]
(λq.T T q) F
[q -> F]
T T F == λa.λb.a λa.λb.a λa.λb.b
[a -> λa.λb.a]
(λb.λa.λb.a)(λa.λb.b)
[b -> λa.λb.b]
(λa.λb.a) == T

c) a = F; b = T

(λp.λq.p p q) F T
[p -> F]
(λq.F F q) T
[q -> T]
F F T == λa.λb.b λa.λb.b T
[a-> λa.λb.b]
(λb.b) T
[b -> T]
λa.λb.a == T

d) a = F; b = F

(λp.λq.p p q)F F
[p -> F]
(λq.F F q) F
[q -> F]
F F F
[a -> λa.λb.b]
(λb.b) F
[b -> F]
λa.λb.b == F

## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

 if (a is true)
	return false;
 if (a is false)
	return true;

if statement = (λb.λa.λb.a λa.λb.b b) //If T replaces b then we get TFT, true passes the false, which passes the true.
				      //If F replaces b then we get TFF, true passes the false, which passes the false.

Since were doing the opposite of this, we need to add an element to the If statement in order to reverse it.
(λa.λb λa.λb.a λa.λb.b b a F T)	      //Now if T is passed, there will be a b TFT ->the T gets passed to the b and we end up with TTF, which reduces to F
				      //Alternatively if the F is passed, we'll get b FFT then TFF, which will reduce to T



## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.
